﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerSound : MonoBehaviour
{
    private AudioSource bgMusic;

    void Awake()
    {
        bgMusic = GetComponent<AudioSource>();
    }

    private void OnCollisionEnter(Collision collision) {

        if (collision.gameObject.tag == "Player") {

            bgMusic.Play();

        }

    }
}
