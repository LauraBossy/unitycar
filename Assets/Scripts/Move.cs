﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using UnityEngine;
using UnityEngine.UIElements;

public class Move : MonoBehaviour {

    //à quoi sert myTransform vu qu'on peut récupérer l'objet direct avce transform ?
    //private Transform myTransform { get; set; }
    private float speed = 8f;
    public float positionx;
    public float reactivityToTurn = 4f;
    public float acceleration = 7f;

    public enum PlayerId { None, Player1, Player2 }; 

    public PlayerId playerId = PlayerId.None;

    void Start() {

        //myTransform = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        //float newX = myTransform.position.x;
        float newX = transform.position.x;

        if (playerId != PlayerId.None) {

            if (playerId == PlayerId.Player1) {

                if (Input.GetKey(KeyCode.LeftArrow)) {
                    newX -= reactivityToTurn * Time.deltaTime;
                }
                else if (Input.GetKey(KeyCode.RightArrow)) {
                    newX += reactivityToTurn * Time.deltaTime;
                }
                else if (Input.GetKey(KeyCode.UpArrow)) {
                    speed += acceleration * Time.deltaTime;
                }
                else if (Input.GetKey(KeyCode.DownArrow)) {
                    speed -= acceleration * Time.deltaTime;
                }

            }
            else {

                if (Input.GetKey(KeyCode.Q)) {
                    newX -= reactivityToTurn * Time.deltaTime;
                }
                else if (Input.GetKey(KeyCode.D)) {
                    newX += reactivityToTurn * Time.deltaTime;
                }
                else if (Input.GetKey(KeyCode.Z)) {
                    speed += acceleration * Time.deltaTime;
                }
                else if (Input.GetKey(KeyCode.S)) {
                    speed -= acceleration * Time.deltaTime;
                }

            }
        }

        //float newZ = myTransform.position.z;
        float newZ = transform.position.z;
        newZ += speed * Time.deltaTime;

        //myTransform.position = new Vector3(newX, 0, newZ);
        transform.position = new Vector3(newX, 0, newZ);
    }
}
