﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{
    private Rigidbody myRigidBody;
    private Vector3 forceToApply = Vector3.zero;
    public float forceFactor = 10f;

    void Awake() { 

        myRigidBody= gameObject.GetComponent<Rigidbody>();

    }

    void Update() {

        float x = 0;
        float z = 0;

        if (Input.GetKey(KeyCode.LeftArrow)) {

            x -= 1f;

        }
        if (Input.GetKey(KeyCode.RightArrow)) {

            x += 1f;

        }
        if (Input.GetKey(KeyCode.DownArrow)) {

            z -= 1f;

        }
        if (Input.GetKey(KeyCode.UpArrow)) {

            z += 1f;

        }

        forceToApply = new Vector3(x, 0, z) * forceFactor;

    }

    void FixedUpdate() {

        myRigidBody.AddForce(forceToApply, ForceMode.Force);

    }
}
